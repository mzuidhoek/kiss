﻿using KISS.Models;
using KISS.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Services
{
    public class OrderServices
    {
        private static OrderRepo orderRepo = new OrderRepo();

        public static async Task MakeOrder(Order order)
        {
            if (await LoginServices.Login(order.Group_Id, order.User_Id))
            {
                await orderRepo.Create(order);
            }
            else
            {
                throw new InvalidDataException("Group or user unkown.");
            }
        }

        public static async Task CompleteOrder(Order order)
        {
            await orderRepo.Delete(order);
        }

        public static async Task CompleteAllOrders(int GroupId)
        {
            foreach(Order order in await orderRepo.ReadByGroup(GroupId))
            {
                await CompleteOrder(order);
            }
        }
    }
}
