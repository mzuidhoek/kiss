﻿using KISS.Models;
using KISS.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Services
{
    public class AccountServices
    {
        private static OrderRepo orderRepo = new OrderRepo();

        public static async Task<IEnumerable<Order>> GetMyCurrentOrders(int UserId)
        {
            return await orderRepo.ReadByUser(UserId);
        }

        public static async Task RemoveMyOrder(Order order)
        {
            await orderRepo.Delete(order);
        }

        public static async Task RemoveAllMyOrders(int UserId)
        {
            foreach(Order order in await GetMyCurrentOrders(UserId))
            {
                await RemoveMyOrder(order);
            }
        }
    }
}
