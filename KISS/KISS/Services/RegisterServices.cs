﻿using KISS.Models;
using KISS.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Services
{
    public class RegisterServices
    {
        private static GroupRepo groupRepo = new GroupRepo();

        private static UserRepo userRepo = new UserRepo();

        public static async Task RegisterNewGroup(Group group, User user)
        {
            await RegisterGroup(group);
            user.Group_Id = group.Id;
            await RegisterUser(user);
        }

        public static async Task RegisterExistingGroup(User user)
        {
            await RegisterUser(user);
        }

        public static async Task<Group> FindGroup(string name)
        {
            return await groupRepo.ReadByName(name);
        }

        private static async Task RegisterGroup(Group group)
        {
            if (await groupRepo.ReadByName(group.Name) == null)
            {
                await groupRepo.Create(group);
            }
            else
            {
                throw new InvalidDataException("A group with this name already exist!");
            }
        }

        private static async Task RegisterUser(User user)
        {
            if (await userRepo.ReadByDisplayname(user) == null)
            {
                await userRepo.Create(user);
            }
            else
            {
                throw new InvalidDataException("A user with this displayname already exist!");
            }
        }
    }
}
