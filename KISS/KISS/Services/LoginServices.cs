﻿using KISS.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Services
{
    public class LoginServices
    {
        public static async Task<bool> Login(int Group_Id, int User_Id)
        {
            GroupRepo groupRepo = new GroupRepo();
            if (await groupRepo.ReadOne(Group_Id) != null)
            {
                UserRepo userRepo = new UserRepo();
                if (await userRepo.ReadOne(User_Id) != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
