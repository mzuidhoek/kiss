﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Models
{
    public class Order : BaseModel
    {        
        public int Group_Id { get; set; }

        public int User_Id { get; set; }

        public string ProductName { get; set; }
    }
}
