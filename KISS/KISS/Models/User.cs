﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Models
{
    public class User : BaseModel
    {
        public int Group_Id { get; set; }

        public string Displayname { get; set; }
    }
}
