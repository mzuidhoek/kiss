﻿using KISS.Models;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Repositories
{
    internal class BaseRepo<TEntity> where TEntity : BaseModel
    {
        private static MobileServiceClient MobileService = new MobileServiceClient("https://kiss.azure-mobile.net/", "BDWZpEvsWTXqgMVVQocQXlbhrKfhzk84");

        internal IMobileServiceTable<TEntity> Table { get { return MobileService.GetTable<TEntity>(); } }

        public async Task Create(TEntity entity)
        {
            await Table.InsertAsync(entity);
        }

        public async Task<IEnumerable<TEntity>> ReadAll()
        {
            return await Table.ReadAsync();
        }

        public async Task<TEntity> ReadOne(int Id)
        {
            return await Table.LookupAsync(Id);
        }

        public async Task Update(TEntity entity)
        {
            await Table.UpdateAsync(entity);
        }

        public async Task Delete(TEntity entity)
        {
            await Table.DeleteAsync(entity);
        }
    }
}
