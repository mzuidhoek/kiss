﻿using KISS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Repositories
{
    internal class GroupRepo : BaseRepo<Group>
    {
        internal async Task<Group> ReadByName(string name)
        {
            var foundResults = await Table.Where(a => a.Name.ToLower() == name.ToLower()).ToEnumerableAsync();
            return foundResults.FirstOrDefault();
        }
    }
}
