﻿using KISS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KISS.Repositories
{
    internal class OrderRepo : BaseRepo<Order>
    {
        internal async Task<IEnumerable<Order>> ReadByGroup(int GroupId)
        {
            return await Table.Where(o => o.Group_Id == GroupId).ToEnumerableAsync();
        }

        internal async Task<IEnumerable<Order>> ReadByUser(int UserId)
        {
            return await Table.Where(o => o.User_Id == UserId).ToEnumerableAsync();
        }
    }
}
