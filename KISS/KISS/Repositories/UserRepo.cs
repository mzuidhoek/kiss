﻿using KISS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

namespace KISS.Repositories
{
    internal class UserRepo : BaseRepo<User>
    {
        internal async Task<IEnumerable<User>> ReadByGroup(int GroupId)
        {
            return await Table.Where(a => a.Group_Id == GroupId).ToEnumerableAsync();
        }

        internal async Task<object> ReadByDisplayname(User user)
        {
            var foundResults = await Table.Where(u => u.Group_Id == user.Group_Id && u.Displayname.ToLower() == user.Displayname.ToLower()).ToEnumerableAsync();
            return foundResults.FirstOrDefault();
        }
    }
}
